require 'legion/extensions/rachio/version'

module Legion
  module Extensions
    module Rachio
      extend Legion::Extensions::Core if Legion::Extensions.const_defined? :Core
    end
  end
end
